import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Board extends React.Component {
    renderSquare(i) {
        return (
            <button className="square" onClick={() => this.props.onClick(i)} >
                {this.props.squares[i]}
            </button>
        );
    }

    render() {
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [{ squares: Array(9).fill(null) }],
            stepNumber: 0,
            xTurn: true
        };
    }

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const squares = history[history.length - 1].squares.slice();

        if (squares[i] == null && !calculateWinner(squares)) {
            squares[i] = this.state.xTurn ? 'X' : 'O';
            const stepNumber = history.length;
            this.setState({ 
                history: history.concat({ squares }), 
                stepNumber, 
                xTurn: !this.state.xTurn 
            });
        }
    }

    jumpTo(moveId) {
        this.setState({ 
            stepNumber: moveId, 
            xTurn: moveId % 2 === 0 
        });
    }

    render() {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[this.state.stepNumber];

        const winner = calculateWinner(current.squares);
        const status = (winner == null ? `Next player: ${this.state.xTurn ? 'X' : 'O'}` : `Winner is ${winner}`);

        const moves = history.map((square, id) => {
            const text = id ? `Go to move ${id}` : `Go to game start`;
            return (
                <li key={id}>
                    <button onClick={() => this.jumpTo(id)}>{text}</button>
                </li>
            )
        })

        return (
            <div className="game">
                <div className="game-board">
                    <Board 
                        onClick={(i) => this.handleClick(i)}
                        squares={current.squares} />
                </div>
                <div className="game-info">
                    <div>{ status }</div>
                    <ol>{ moves }</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}